# RegressyCommon

RegressyCommonは、[Regressy](https://regressy.fluxware.jp)でe2eテストを構築するためのライブラリです。

Ruby言語とtest-unitフレームワークを利用して、e2eテストを構築します。

## インストール

Gemfileに追加し、bundleを実行します。

```ruby
gem 'regressy_common'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install regressy_common

## 使い方

RegressyCommonは、ページオブジェクトモデルに基づくe2eテスト構築を支援するライブラリです。

ページオブジェクト、テストセット、テストスイートでテスト全体を構成します。

### ディレクトリ構成

ディレクトリ構成の例を以下に示します。

```
your-e2e-project
├── app
│   ├── pages        # ここにページオブジェクトを格納します。
│   ├── test_sets    # ここにテストセットを格納します。
│   ├── test_suites  # ここにテストスイートを格納します。
│   :
└── config
```

- ページオブジェクト: テスト対象サイトのページに対応するオブジェクト。
  1ページに対して1オブジェクトが対応します。
- テストセット: 関連した一連のテストの集合。
  test-unit における TestCaseに相当。
  ここで定義したテストからページオブジェクトを操作し、テストを実行します。
- テストスイート: 関連した一連のテストセットの集合。
  ここで定義したテストスイートから、関連するテストセットを操作します。

## ページオブジェクト

- ページを司るオブジェクトです。
- テスト対象のサイトを構成する1ページに対して1オブジェクトを定義します。
- 以下にページオブジェクトの基底クラスを例示します。

```ruby
require 'test/unit'
require 'regressy_common'
module Pages
  class Base
    include Test::Unit::Assertions
    include RegressyCommon::Checker
    include RegressyCommon::Clicker
    include RegressyCommon::Sender
    include RegressyCommon::Selector

    def initialize(test_case)
      @test_case   = test_case
      @driver      = test_case.driver
      @target_host = test_case.target_host
    end

    def logger
      RegressyCommon::Utils::StandardLogger.instance
    end

    def take_snap(tag=nil)
      tag ||= %(#{self.class.name}##{caller_locations(1).first.label})
      RegressyCommon::SnapShot.new(@driver, tag).take_and_store
    end

    def set_window_size
      @driver.manage.window.resize_to(1600, 1024)
    end

    def access_to_target_page
      @driver.get(page_url)
      assert_element_exists(:xpath, xpath_page_title)
    end

    protected

    def page_url
      raise "should implement this method"
    end
    def xpath_page_title
      raise "should implement this method"
    end
  end
end
```

- そして、基底クラスを継承するページクラス。

```ruby
require 'pages/base'
class Pages::Index < Pages::Base

  def execute_search(word)
    assert_element_present(:xpath, xpath_search_input )
    send_keys(:xpath, xpath_search_input, word)
    retry_click(:xpath, xpath_search_submit)
    assert_element_present(:xpath, xpath_search_result)
  end

  protected

  def page_url
    %(#{@test_case.target_host}/)
  end
  def xpath_page_title
    %(//title[contains(text(), "Yahoo! JAPAN")])
  end
  def xpath_search_input
    %(//input[@type="search"])
  end
  def xpath_search_submit
    %(//button[@type="submit"])
  end
  def xpath_search_result
    %(//a[contains(@href, "regressy")])
  end
end
```

## テストセット

- テストの例。
```ruby
require 'regressy_common'
require 'pages/index'
class SampleTest < RegressyCommon::TestSets::Base
  def setup
    @target_host = 'https://yahoo.co.jp'
    super
  end
  def test_01_search
    index_page = Pages::Index.new(self)
    index_page.access_to_target_page
    index_page.take_snap

    index_page.execute_search("regressy")
  end
end
```

## テストスイート

- テストスイートの例。
```
require 'test/unit'
require 'test/unit/testsuite'
require 'test/unit/ui/console/testrunner'
require 'test_sets/sample_test'

regression_test = Test::Unit::TestSuite.new("regression test")
regression_test << SampleTest.suite  # default local chrome
regression_test << SampleTest.suite_with_capability(:local_firefox)

Test::Unit::UI::Console::TestRunner.run(regression_test)
```
ここで、ブラウザの種類を指定します。

## テストの実行

```
# ruby app/test_suites/regression_test.rb
Loaded suite regression test
Started
..
Finished in 33.660497466 seconds.
-----------------------------------------------------------------------------------------------------------------------
2 tests, 8 assertions, 0 failures, 0 errors, 0 pendings, 0 omissions, 0 notifications
100% passed
-----------------------------------------------------------------------------------------------------------------------
0.06 tests/s, 0.24 assertions/s
```

## ページオブジェクトで使用する共通機能

- checker
- clicker
- sender
- selector


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/regressy_common. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the RegressyCommon project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/regressy_common/blob/master/CODE_OF_CONDUCT.md).
