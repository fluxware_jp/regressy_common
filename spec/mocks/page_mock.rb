class PageMock
  include RegressyCommon::Checker
  include RegressyCommon::Clicker
  include RegressyCommon::Selector
  include RegressyCommon::Sender

  def initialize(test_case)
    @test_case   = test_case
    @driver      = test_case.driver
    @target_host = test_case.target_host
  end

  def logger
    Logger.new(STDOUT)
  end
end
