class SeleniumWebDriverMock
  attr_accessor(:implicit_wait, :params)
  def manage
    self
  end

  def timeouts
    self
  end

  def find_element(how, what)
    (what =~ /^success-xpath/)? SuccessElement.new(how, what) : nil
  end

  def find_elements(how, what)
    (what =~ /^success-xpath/)? [SuccessElement.new(how, what)] : []
  end

  def execute_script(*params)
  end

  def resize_to(*params)
    true
  end

  def window
    self
  end

  private

  class SuccessElement
    def initialize(how, what)
      @how = how
      @what = what
    end
    def present?
      true
    end
    def displayed?
      true
    end
    def enabled?
      true
    end
    def click
      (@what =~ /click-success/)? true : raise('Exception with click')
    end
    def send_keys(content)
    end
  end
end
