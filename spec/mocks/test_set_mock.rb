class TestSetMock
  attr_accessor(:driver, :target_host)

  def initialize
    @driver = SeleniumWebDriverMock.new
    @target_host = ''
  end

  def assert_block(message)
    (yield)? true : raise('Assert error')
  end

  def logger
    Logger.new(STDOUT)
  end
end
