require "bundler/setup"
require "test/unit"
require "webmock/rspec"
require "selenium-webdriver"
require "regressy_common"
require "spec/mocks/page_mock"
require "spec/mocks/test_set_mock"
require "spec/mocks/selenium_web_driver_mock"
require "spec/mocks/selenium_web_driver_support_select"
require "spec/mocks/appium_driver_mock"
require "pry"

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
