require "spec_helper"
require "lib/stabs/selenium-web_driver"
module RegressyCommon
  RSpec.describe SnapShot do
    let(:tag) { 'dummy_tag_12345' }
    let(:hostname) { 'dev.regressy.fluxware.jp' }
    let(:path) { '/api/v1/test_task/#{test_task_id}/snapshot' }
    let(:test_task_id) { '1234' }
    let(:api_token) { 'api_token' }
    let(:driver) do
      Stabs::Selenium::WebDriver.new
    end
    before do
      ENV['API_BASE'] = %(https://#{hostname})
      ENV['TEST_TASK_ID'] = test_task_id
      ENV['API_TOKEN'] = api_token
      WebMock.allow_net_connect!
      stub_request(:post, "https://#{hostname}/user/repos")
        .with(headers: { "Authorization" => "token 1234abcd" })
        .to_return(body: "hoge")
    end
    let!(:snap_shot) { SnapShot.new(driver, tag) }
    let!(:snap_shot_non_tag) { SnapShot.new(driver, '')}
    describe '#snap_file' do
      let!(:snap_file) { snap_shot.send(:snap_file) }
      it 'created a snap_file' do
        expect(File.exists?(snap_file.path)).to eq true
      end
      after do
        File.unlink(snap_file.path)
      end
    end
    describe '#snap_file_prefix' do
      context 'when exists tag' do
        it 'snap + tag' do
          expect(snap_shot.send(:snap_file_prefix)).to eq %(snap_#{tag}_)
        end
      end
      context 'when not exists tag' do
        it 'only snap' do
          expect(snap_shot_non_tag.send(:snap_file_prefix)).to eq %(snap_)
        end
      end
    end
    describe '#save_screen_shot' do
      let!(:snap_file) { snap_shot.send(:snap_file) }
      before do
        snap_shot.send(:save_screenshot)
      end
      it 'saved screen_shot to snap_file' do
        expect(File.size(snap_file.path)).to be > 0
      end
      after do
        File.unlink(snap_file.path)
      end
    end
    describe '#take_and_store' do
      it 'post a snap_shot file' do
        expect(snap_shot.take_and_store).to eq true
      end
    end
  end
end
