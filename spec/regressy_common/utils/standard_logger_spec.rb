require "spec_helper"
module RegressyCommon
  module Utils
    RSpec.describe StandardLogger do
      describe '#instance' do
        it 'logger instance' do
          expect(RegressyCommon::Utils::StandardLogger.instance.instance_of?(Logger)).to eq true
        end
      end
    end
  end
end
