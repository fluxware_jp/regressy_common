require "spec_helper"
module RegressyCommon
  RSpec.describe SnapStorage do
    let(:hostname) { 'dev.regressy.fluxware.jp' }
    let(:test_task_id) { '1234' }
    let(:api_token) { 'api_token' }
    let(:snap_storage) { SnapStorage.new }
    let(:api_url) { %(https://#{hostname}/inner_api/v1/test_tasks/#{test_task_id}/snap_shots) }
    let(:snap_tag) { 'tag_12345' }
    before do
      @snap_file = Tempfile.create(['snap_', '.png'])
      ENV['API_BASE'] = %(https://#{hostname}/inner_api/v1)
      ENV['TEST_TASK_ID'] = test_task_id
      ENV['API_TOKEN'] = api_token
      WebMock.allow_net_connect!
      stub_request(:post, api_url).to_return do |request|
        {
          body: request.body,
        }
      end
    end
    after do
      File.unlink(@snap_file.path) if File.exists?(@snap_file.path)
    end
    describe '#ENV variables' do
      it 'API_TOKEN' do
        expect(snap_storage.send(:api_token)).to eq api_token
      end
      it 'TEST_TASK_ID' do
        expect(snap_storage.send(:test_task_id)).to eq test_task_id
      end
      it 'API_BASE' do
        expect(snap_storage.send(:api_base)).to eq %(https://#{hostname}/inner_api/v1)
      end
    end
    describe '#path' do
      it 'path is path' do
        expect(snap_storage.send(:path)).to eq %(/test_tasks/#{test_task_id}/snap_shots)
      end
    end
    describe '#inner_api_url' do
      it 'url is correct url' do
        expect(snap_storage.send(:inner_api_url)).to eq api_url
      end
    end
    describe '#post_file' do
      let(:response) { snap_storage.send(:post_file, @snap_file, snap_tag) }
      it 'exists response' do
        expect(response.instance_of?(HTTP::Message)).to eq true
      end
    end
    describe '#store_file' do
      it 'file unlinked' do
        expect(snap_storage.store_file(@snap_file, snap_tag)).to be == 1
      end
    end
  end
end
