require "spec_helper"
module RegressyCommon
  RSpec.describe DriverCreator do
    let(:selenium_hub_url) { %(http://selenium-hub:4444/wd/hub)}
    let(:capability_name_smartphone) { :android_10_0_pixel_3}
    let(:capability_name_win10_ie11) { :win10_ie11}
    let(:capability_name_local_ff) { :local_firefox}
    let(:capability_smartphone) { RegressyCommon::Capabilities::Capability.new(capability_name_smartphone) }
    let(:capability_win10_ie11) { RegressyCommon::Capabilities::Capability.new(capability_name_win10_ie11) }
    let(:capability_local_ff) { RegressyCommon::Capabilities::Capability.new(capability_name_local_ff) }
    let(:driver_creator_smartphone) { RegressyCommon::DriverCreator.new(capability_smartphone) }
    let(:driver_creator_win10_ie11) { RegressyCommon::DriverCreator.new(capability_win10_ie11) }
    let(:driver_creator_local_ff) { RegressyCommon::DriverCreator.new(capability_local_ff) }
    let(:driver_creator_local) { RegressyCommon::DriverCreator.new(nil) }
    before do
      stub_request(:post, "http://selenium-hub:4444/wd/hub/session").
        with(
          headers: {
            'Accept'=>'application/json',
          }).
        to_return(
          status: 200,
          body: "",
          headers: {},
        )
      allow(Selenium::WebDriver).to receive(:for).and_return(SeleniumWebDriverMock.new)
      allow(Appium::Driver).to receive(:new).and_return(AppiumDriverMock.new)
    end
    describe '#selenium_hub_url' do
      it 'success case' do
        expect(driver_creator_smartphone.send(:selenium_hub_url)).to eq selenium_hub_url
      end
    end
    describe '#options_chrome' do
      it 'instance_of' do
        expect(driver_creator_smartphone.send(:options_chrome).instance_of?(Selenium::WebDriver::Chrome::Options)).to eq true
      end
      it 's3c option' do
        expect(driver_creator_smartphone.send(:options_chrome).options[:w3c]).to eq false
      end
    end
    describe '#http_client' do
      it 'instance_of Selenium::WebDriver::Remote::Http::Default' do
        expect(driver_creator_smartphone.send(:http_client).instance_of?(Selenium::WebDriver::Remote::Http::Default)).to eq true
      end
    end
    describe '#driver_remote_selenium' do
      it 'instance_of SeleniumWebDriverMock' do
        expect(driver_creator_smartphone.send(:driver_remote_selenium).instance_of?(SeleniumWebDriverMock)).to eq true
      end
    end
    describe '#driver_remote_appium' do
      it 'instance_of AppiumDriverMock' do
        expect(driver_creator_smartphone.send(:driver_remote_appium).instance_of?(AppiumDriverMock)).to eq true
      end
    end
    describe '#create_driver_remote' do
      context 'when smartphone' do
        it 'instance_of AppiumDriverMock' do
          expect(driver_creator_smartphone.send(:create_driver_remote).instance_of?(AppiumDriverMock)).to eq true
        end
      end
      context 'when not smartphone' do
        it 'instance_of SeleniumWebDriverMock' do
          expect(driver_creator_win10_ie11.send(:create_driver_remote).instance_of?(SeleniumWebDriverMock)).to eq true
        end
      end
    end
    describe '#local_browser_name' do
      context 'when firefox' do
        it 'sym firefox' do
          expect(driver_creator_local_ff.send(:local_browser_name)).to eq :firefox
        end
      end
      context 'when default' do
        it 'sym chrome' do
          expect(driver_creator_local.send(:local_browser_name)).to eq :chrome
        end
      end
    end
    describe '#create_driver_local' do
      context 'when firefox' do
        it 'instance_of SeleniumWebDriverMock' do
          expect(driver_creator_local_ff.send(:create_driver_local).instance_of?(SeleniumWebDriverMock)).to eq true
        end
      end
      context 'when default' do
        it 'instance_of SeleniumWebDriverMock' do
          expect(driver_creator_local.send(:create_driver_local).instance_of?(SeleniumWebDriverMock)).to eq true
        end
      end
    end
    describe '#create_driver' do
      context 'when remote' do
        it 'instance_of AppiumDriverMock' do
          expect(driver_creator_smartphone.send(:create_driver_remote).instance_of?(AppiumDriverMock)).to eq true
        end
      end
      context 'when local' do
        it 'instance_of SeleniumWebDriverMock' do
          expect(driver_creator_local.send(:create_driver_local).instance_of?(SeleniumWebDriverMock)).to eq true
        end
      end
    end
    describe '#initialize' do
      it 'instance_of DriverCreator' do
          expect(RegressyCommon::DriverCreator.new(capability_name_smartphone).instance_of?(RegressyCommon::DriverCreator)).to eq true
      end
    end
  end
end
