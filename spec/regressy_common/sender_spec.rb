require "spec_helper"
module RegressyCommon
  RSpec.describe Sender do
    let(:sender_test_page) do
      PageMock.new(TestSetMock.new)
    end
    let(:send_content) { "abcdefghijklmnopqrstuvwxyz1234567890" }

    describe '#target_element' do
      it 'success case' do
        expect do
          sender_test_page.send(
            :target_element,
            :xpath,           # how
            'success-xpath'   # what
          )
        end.to_not raise_error
      end
    end

    describe '#send_keys' do
      it 'success case' do
        expect do
          sender_test_page.send_keys(
            :xpath,           # how
            'success-xpath',  # what
            send_content      # content
          )
        end.to_not raise_error
      end
    end

    describe '#jsend_keys' do
      it 'success case' do
        expect do
          sender_test_page.jsend_keys(
            :xpath,           # how
            'success-xpath',  # what
            send_content      # content
          )
        end.to_not raise_error
      end
    end

    describe '#jsend_keys_with_keyinput' do
      it 'success case' do
        expect do
          sender_test_page.jsend_keys_with_keyinput(
            :xpath,           # how
            'success-xpath',  # what
            send_content      # content
          )
        end.to_not raise_error
      end
    end
  end
end
