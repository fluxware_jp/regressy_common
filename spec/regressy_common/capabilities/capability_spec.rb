require "spec_helper"
module RegressyCommon
  module Capabilities
    RSpec.describe Capability do
      let(:capability_name) { :android_10_0_pixel_3 }
      let(:capability) { Capability.new(capability_name) }
      let(:local_capability) { Capability.new(:local_firefox) }
      describe '#accesser' do
        it 'capability_name' do
          expect(capability.capability_name).to eq capability_name
        end
        it 'caps' do
          expect(capability.caps).to eq RegressyCommon::Capabilities.send(capability_name)
        end
      end
      describe '#is_local?' do
        context 'when defined local' do
          it 'true' do
            expect(local_capability.is_local?).to eq true
          end
        end
        context 'when not define local' do
          it 'false' do
            expect(capability.is_local?).to eq false
          end
        end
      end
      describe '#is_smartphone?' do
        context 'when defined local' do
          it 'smartphone' do
            expect(capability.is_smartphone?).to eq true
          end
        end
        context 'when not define local' do
          it 'not smartphone' do
            expect(local_capability.is_smartphone?).to eq false
          end
        end
      end
    end
  end
end
