require "spec_helper"
module RegressyCommon
  module TestSets
    RSpec.describe Base do
      let(:capability_name_smartphone) { :android_10_0_pixel_3}
      let(:capability_name_win10_ie11) { :win10_ie11}
      let(:capability_name_local_ff) { :local_firefox}
      let(:capability_smartphone) { RegressyCommon::Capabilities::Capability.new(capability_name_smartphone) }
      let(:capability_win10_ie11) { RegressyCommon::Capabilities::Capability.new(capability_name_win10_ie11) }
      let(:capability_local_ff) { RegressyCommon::Capabilities::Capability.new(capability_name_local_ff) }
      let(:test_instance) { RegressyCommon::TestSets::Base.new('test') }
      let(:test_instance_smartphone) do
        instance = RegressyCommon::TestSets::Base.new('test')
        instance.capability = capability_smartphone
        instance
      end
      before do
        allow(Selenium::WebDriver).to receive(:for).and_return(SeleniumWebDriverMock.new)
        allow(Appium::Driver).to receive(:new).and_return(AppiumDriverMock.new)
      end
      describe '#setup' do
        it 'success case' do
          test_instance.setup
          expect(test_instance.driver.instance_of?(SeleniumWebDriverMock)).to eq true
        end
      end
      describe '#set_window_size' do
        context 'when smartphone' do
          it 'not called' do
            test_instance.setup
            expect(test_instance_smartphone.send(:set_window_size)).to eq nil
          end
        end
        context 'when not smartphone' do
          it 'called' do
            test_instance.setup
            expect(test_instance.send(:set_window_size)).to eq true
          end
        end
      end
      describe '#set_driver' do
        it 'driver instance_of SeleniumWebDriverMock' do
          test_instance.send(:set_driver)
          expect(test_instance.driver.instance_of?(SeleniumWebDriverMock)).to eq true
        end
      end
      describe '#is_smartphone?' do
        context 'when smartphone' do
          it 'returned true' do
            expect(test_instance_smartphone.is_smartphone?).to eq true
          end
        end
        context 'when not smartphone' do
          it 'returned false' do
            expect(test_instance.is_smartphone?).to eq false
          end
        end
      end
      describe '#define_capability' do
        it 'exists required_capability, and return instance_of Capability' do
          test_instance.define_capability(capability_name_smartphone)
          test_instance.required_capability
          expect(test_instance.capability.instance_of?(RegressyCommon::Capabilities::Capability)).to eq true
        end
      end
      describe '#teardown' do
        it 'return nil' do
          expect(test_instance.teardown).to eq nil
        end
      end
      describe '#self.suite_with_capability' do
        it 'instance_of Test::Unit::TestSuite' do
          result = RegressyCommon::TestSets::Base.suite_with_capability(capability_name_smartphone)
          expect(result.instance_of?(Test::Unit::TestSuite)).to eq true
        end
      end
      describe '#self.define_capability' do
        it 'exists self.required_capability, and return instance_of Capability' do
          RegressyCommon::TestSets::Base.define_capability(capability_name_smartphone)
          instance = RegressyCommon::TestSets::Base.required_capability
          expect(instance.instance_of?(RegressyCommon::Capabilities::Capability)).to eq true
        end
      end
      describe '#logger' do
        it 'instance_of Logger' do
          expect(test_instance.send(:logger).instance_of?(Logger)).to eq true
        end
      end
    end
  end
end
