require "spec_helper"
module RegressyCommon
  RSpec.describe Selector do
    let(:selector_test_page) do
      PageMock.new(TestSetMock.new)
    end

    describe '#select_option_by_text' do
      it 'success case' do
        result = selector_test_page.select_option_by_text(
          :xpath,           # how
          'success-xpath',  # what
          'test_content',   # content
        )
        puts result
      end
    end
  end
end
