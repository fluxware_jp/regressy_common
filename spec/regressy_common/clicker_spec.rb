require "spec_helper"
module RegressyCommon
  RSpec.describe Clicker do
    let(:click_test_page) do
      PageMock.new(TestSetMock.new)
    end
    let(:message_content) { "failure message" }

    describe '#failure_message' do
      it 'create message' do
        expect(click_test_page.send(:failure_message, message_content)).to include message_content
      end
    end

    describe '#click_with_rescue' do
      it 'success case' do
        result = click_test_page.click_with_rescue(
          :xpath,
          'success-xpath_click-success'
        )
        expect(result).to eq true
      end
      it 'fail case' do
        result = click_test_page.click_with_rescue(
          :xpath,
          'success-xpath_click-fail'
        )
        expect(result).to eq false
      end
    end

    describe '#retry_click' do
      it 'success case' do
        expect do
          click_test_page.retry_click(
            :xpath,
            'success-xpath_click-success'
          )
        end.to_not raise_error
      end
      it 'fail case' do
        expect do
          click_test_page.retry_click(
            :xpath,
            'success-xpath_click-fail',
            1
          ) do
            puts 'execute block'
          end
        end.to raise_error(Selenium::WebDriver::Error::UnknownError)
      end
    end
  end
end
