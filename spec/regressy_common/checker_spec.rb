require "spec_helper"
module RegressyCommon
  RSpec.describe Checker do
    let(:checker_test_page) do
      PageMock.new(TestSetMock.new)
    end
    let(:message_content) { "failure message" }

    describe '#failure_message' do
      it 'create message' do
        expect(checker_test_page.send(:failure_message, message_content)).to include message_content
      end
    end

    describe '#assert_with_timeout' do
      it 'success case' do
        result = checker_test_page.send(:assert_with_timeout, message_content) do
          true
        end
        expect(result).to eq true
      end
      it 'fail case' do
        expect do
          checker_test_page.send(:assert_with_timeout, message_content, 1) do
            false
          end
        end.to raise_error('Assert error')
      end
    end

    describe '#assert_elements_check_method' do
      it 'success case' do
        result = checker_test_page.send(
          :assert_elements_check_method,
          :xpath,           # how
          'success-xpath',  # what
          0,                # index
          :present?,        # check_method
          message_content   # message
        )
        expect(result).to eq true
      end
      it 'fail case' do
        expect do
          checker_test_page.send(
            :assert_elements_check_method,
            :xpath,           # how
            'fail-xpath',     # what
            0,                # index
            :present?,        # check_method
            message_content,  # message
            1,                # wait
          )
        end.to raise_error('Assert error')
      end
    end

    describe '#element_present_no_assert?' do
      it 'success case' do
        result = checker_test_page.element_present_no_assert?(
          :xpath,           # how
          'success-xpath',  # what
          1,                # wait
        )
        expect(result).to eq true
      end
      it 'fail case' do
        result = checker_test_page.element_present_no_assert?(
          :xpath,           # how
          'fail-xpath',     # what
          1,                # wait
        )
        expect(result).to eq false
      end
    end

    describe '#element_exists_no_assert?' do
      it 'success case' do
        result = checker_test_page.element_exists_no_assert?(
          :xpath,           # how
          'success-xpath',  # what
          1,                # wait
        )
        expect(result).to eq true
      end
      it 'fail case' do
        result = checker_test_page.element_exists_no_assert?(
          :xpath,           # how
          'fail-xpath',     # what
          1,                # wait
        )
        expect(result).to eq false
      end
    end

    describe '#assert_element_exists' do
      it 'success case' do
        result = checker_test_page.assert_element_exists(
          :xpath,           # how
          'success-xpath',  # what
          1,                # wait
        )
        expect(result).to eq true
      end
      it 'fail case' do
        expect do
          checker_test_page.assert_element_exists(
            :xpath,           # how
            'fail-xpath',     # what
            1,                # wait
          )
        end.to raise_error('Assert error')
      end
    end

    describe '#assert_element_present', type: :doing do
      it 'success case' do
        result = checker_test_page.assert_element_present(
          :xpath,           # how
          'success-xpath',  # what
          1,                # wait
        )
        expect(result).to eq true
      end
      it 'fail case' do
        expect do
          checker_test_page.assert_element_present(
            :xpath,           # how
            'fail-xpath',     # what
            1,                # wait
          )
        end.to raise_error('Assert error')
      end
    end

    describe '#assert_elements_present', type: :doing do
      it 'success case' do
        result = checker_test_page.assert_elements_present(
          :xpath,           # how
          'success-xpath',  # what
          0,                # index
          1,                # wait
        )
        expect(result).to eq true
      end
      it 'fail case' do
        expect do
          checker_test_page.assert_elements_present(
            :xpath,           # how
            'fail-xpath',     # what
            0,                # index
            1,                # wait
          )
        end.to raise_error('Assert error')
      end
    end

    describe '#assert_element_enabled', type: :doing do
      it 'success case' do
        result = checker_test_page.assert_element_enabled(
          :xpath,           # how
          'success-xpath',  # what
          1,                # wait
        )
        expect(result).to eq true
      end
      it 'fail case' do
        expect do
          checker_test_page.assert_element_enabled(
            :xpath,           # how
            'fail-xpath',     # what
            1,                # wait
          )
        end.to raise_error('Assert error')
      end
    end

    describe '#assert_elements_enabled', type: :doing do
      it 'success case' do
        result = checker_test_page.assert_elements_enabled(
          :xpath,           # how
          'success-xpath',  # what
          0,                # index
          1,                # wait
        )
        expect(result).to eq true
      end
      it 'fail case' do
        expect do
          checker_test_page.assert_elements_enabled(
            :xpath,           # how
            'fail-xpath',     # what
            0,                # index
            1,                # wait
          )
        end.to raise_error('Assert error')
      end
    end
  end
end
