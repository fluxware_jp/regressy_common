
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "regressy_common/version"

Gem::Specification.new do |spec|
  spec.name          = "regressy_common"
  spec.version       = RegressyCommon::VERSION
  spec.authors       = ["info@fluxware.co.jp"]
  spec.email         = ["info@fluxware.co.jp"]

  spec.summary       = %q{Shared module for regression test at Regressy.}
  spec.homepage      = "https://bitbucket.org/fluxware_jp/regressy_common"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://rubygems.org"

    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = "https://bitbucket.org/fluxware_jp/regressy_common/"
    spec.metadata["changelog_uri"] = "https://bitbucket.org/fluxware_jp/regressy_common/CHANGELOG.md"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "httpclient", "~> 2.8.3"
  spec.add_dependency "activesupport", "~> 6.0.1"
  spec.add_dependency "appium_lib", "~> 10.5.0"
  spec.add_dependency "selenium-webdriver", "~> 3.142.3"
  spec.add_development_dependency "bundler", "~> 1.17"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "pry-byebug", "~> 3.7"
  spec.add_development_dependency "webmock", "~> 3.6.2"
  spec.add_development_dependency "test-unit", "~> 3.3.4"
end
