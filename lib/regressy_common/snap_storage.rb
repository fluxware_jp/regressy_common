require 'httpclient'

module RegressyCommon
  class SnapStorage
    def store_file(file, tag)
      # if not define api_base, skip post and unlink for development
      if api_base
        post_file(file, tag) if api_base
        File.unlink(file.path)
      end
    end

    private

    def post_file(file, tag)
      res = File.open(file.path) do |file|
        HTTPClient.new.post(
          inner_api_url,
          {
            upload: file,
            api_token: api_token,
            snap_tag: tag,
          }
        )
      end
    end

    def inner_api_url
      %(#{api_base}#{path})
    end

    def api_base
      ENV['API_BASE']
    end

    def path
      %(/test_tasks/#{test_task_id}/snap_shots)
    end

    def test_task_id
      ENV['TEST_TASK_ID']
    end

    def api_token
      ENV['API_TOKEN']
    end
  end
end
