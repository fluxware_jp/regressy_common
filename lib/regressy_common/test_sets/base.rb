require 'test/unit'
module RegressyCommon
  module TestSets
    class Base < Test::Unit::TestCase
      attr_accessor(:driver, :capability, :target_host)
      self.test_order = :defined

      def self.define_capability(capability_name)
        singleton_class.send(:define_method, :required_capability, -> do
          @capability = RegressyCommon::Capabilities::Capability.new(capability_name)
        end)
      end

      def self.suite_with_capability(capability_name)
        test_suite = suite
        test_suite.tests.map! do |v|
          v.capability = RegressyCommon::Capabilities::Capability.new(capability_name)
          v
        end
        test_suite
      end

      def setup
        set_driver
        set_window_size
        @accept_next_alert = true
        @verification_errors = []

        # set timezone
        ENV['TZ'] = "Asia/Tokyo"
      end

      def teardown
        @driver&.quit
        assert_equal [], @verification_errors if @verification_errors
      end

      def define_capability(capability_name)
        singleton_class.send(:define_method, :required_capability, -> do
          @capability = RegressyCommon::Capabilities::Capability.new(capability_name)
        end)
        self
      end

      def is_smartphone?
        (@capability)? @capability.is_smartphone? : false
      end

      protected

      def set_driver
        @driver = RegressyCommon::DriverCreator.new(@capability).create_driver
      end

      def set_window_size
        unless is_smartphone?
          @driver&.manage.window.resize_to(1600, 1024)
        end
      end

      def logger
        RegressyCommon::Utils::StandardLogger.instance
      end
    end
  end
end
