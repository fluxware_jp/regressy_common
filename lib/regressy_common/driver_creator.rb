require 'selenium-webdriver'
require 'appium_lib'
module RegressyCommon
  class DriverCreator
    def initialize(capability)
      @capability = capability
    end

    def create_driver
      if @capability && !@capability.is_local?
        create_driver_remote
      else
        create_driver_local
      end
    end

    protected

    def read_timeout_value
      140
    end

    def create_driver_local
      Selenium::WebDriver.for(
        local_browser_name,
        url: selenium_hub_url,
        http_client: http_client,
        options: options_chrome,
      )
    end

    def local_browser_name
      browser_name = @capability && @capability.caps["browserName"]
      (browser_name)? browser_name.to_sym : :chrome
    end

    def create_driver_remote
      (@capability.is_smartphone?)? driver_remote_appium : driver_remote_selenium
    end

    def driver_remote_appium
      Appium::Driver.new(
        {
          'caps' => @capability.caps,
          'appium_lib' => {
            server_url: selenium_hub_url,
          },
        },
        true,
      ).start_driver
    end

    def driver_remote_selenium
      Selenium::WebDriver.for(
        :remote,
        url: selenium_hub_url,
        desired_capabilities: @capability.caps,
        http_client: http_client,
      )
    end

    def http_client
      client = Selenium::WebDriver::Remote::Http::Default.new
      client.read_timeout = read_timeout_value
      client
    end

    def options_chrome
      options = Selenium::WebDriver::Chrome::Options.new
      options.add_option(:w3c, false)
      options
    end

    def selenium_hub_url
      %(http://selenium-hub:4444/wd/hub)
    end
  end
end
