require 'selenium-webdriver'
module RegressyCommon
  module Capabilities
    def self.local_firefox
      caps = Selenium::WebDriver::Remote::Capabilities.new
      caps["name"] = "local_firefox"
      caps["local"] = true
      caps["browserName"] = "firefox"
      caps["record_video"] = true
      caps["timeZone"] = "Tokyo"  # for TestingBot
      caps
    end
  end
end
