Dir[File.expand_path('../cap_*.rb', __FILE__)].each do |file|
  require file
end
module RegressyCommon
  module Capabilities
    class Capability
      attr_accessor(:capability_name, :caps)
      def initialize(capability_name)
        @capability_name = capability_name
        @caps = Capabilities.send(capability_name)
      end

      def is_local?
        !@caps["local"].nil?
      end

      def is_smartphone?
        !@caps["platformName"].nil?
      end
    end
  end
end
