module RegressyCommon
  module Capabilities
    def self.android_10_0_pixel_3
      caps = {}
      caps["name"] = "android_10_0_pixel_3"
      caps["browserName"] = "Chrome"
      caps["platform"] = "ANDROID"
      caps["version"] = "10.0"
      caps["deviceName"] = "Pixel 3"
      caps["platformName"] = "Android"
      caps["record_video"] = true
      caps["timeZone"] = "Tokyo"
      caps
    end
  end
end
