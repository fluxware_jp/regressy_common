module RegressyCommon
  module Capabilities
    def self.real_galaxy_s8
      caps = {}
      caps["browserName"] = "chrome"
      caps["deviceName"] = "Galaxy S8"
      caps["name"] = "real_galaxy_s8"
      caps["platformName"] = "Android"
      caps["realDevice"] = true
      caps["record_video"] = true
      caps["version"] = "9.0"
      caps
    end
  end
end
