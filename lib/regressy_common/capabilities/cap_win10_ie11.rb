require 'selenium-webdriver'
module RegressyCommon
  module Capabilities
    def self.win10_ie11
      caps = Selenium::WebDriver::Remote::Capabilities.new
      caps["name"] = "win10_ie11"
      caps["browserName"] = "internet explorer"
      caps["platform"] = "WIN10"
      caps["version"] = "11"
      caps["record_video"] = true
      caps["cssSelectorsEnabled"] = true
      caps["javascriptEnabled"] = true
      caps["nativeEvents"] = true
      caps["timeZone"] = "Tokyo"  # for TestingBot
      caps
    end
  end
end
