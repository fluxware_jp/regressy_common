module RegressyCommon
  module Capabilities
    def self.android_9_0_galaxy_s9
      caps = {}
      caps["name"] = "android_9_0_galaxy_s9"
      caps["browserName"] = "Chrome"
      caps["platform"] = "ANDROID"
      caps["version"] = "9.0"
      caps["deviceName"] = "Galaxy S9"
      caps["platformName"] = "Android"
      caps["record_video"] = true
      caps["timeZone"] = "Tokyo"  # for TestingBot
      caps
    end
  end
end
