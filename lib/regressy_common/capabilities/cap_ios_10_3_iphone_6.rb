module RegressyCommon
  module Capabilities
    def self.ios_10_3_iphone_6
      caps = {}
      caps["name"] = "ios_10_3_iphone_6"
      caps["browserName"] = "safari"
      caps["platform"] = "SIERRA"
      caps["version"] = "10.3"
      caps["deviceName"] = "iPhone 6"
      caps["platformName"] = "iOS"
      caps["record_video"] = true
      caps["timeZone"] = "Tokyo"
      caps
    end
  end
end
