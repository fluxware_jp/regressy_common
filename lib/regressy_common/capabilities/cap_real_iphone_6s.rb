module RegressyCommon
  module Capabilities
    def self.real_iphone_6s
      caps = {}
      caps["browserName"] = "safari"
      caps["deviceName"] = "iPhone 6s"
      caps["name"] = "real_iphone_6s"
      caps["platformName"] = "iOS"
      caps["realDevice"] = true
      caps["record_video"] = true
      caps["version"] = "12.1"
      caps
    end
  end
end
