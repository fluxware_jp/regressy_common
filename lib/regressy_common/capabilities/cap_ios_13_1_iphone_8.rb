module RegressyCommon
  module Capabilities
    def self.ios_13_1_iphone_8
      caps = {}
      caps["name"] = "ios_13_1_iphone_8"
      caps["browserName"] = "safari"
      caps["platform"] = "CATALINA"
      caps["version"] = "13.1"
      caps["deviceName"] = "iPhone 8"
      caps["platformName"] = "iOS"
      caps["record_video"] = true
      caps["timeZone"] = "Tokyo"  # for TestingBot
      caps
    end
  end
end
