module RegressyCommon
  module Capabilities
    def self.ios_11_4_iphone_7
      caps = {}
      caps["name"] = "ios_11_4_iphone_7"
      caps["browserName"] = "safari"
      caps["platform"] = "HIGH-SIERRA"
      caps["version"] = "11.4"
      caps["deviceName"] = "iPhone 7"
      caps["platformName"] = "iOS"
      caps["record_video"] = true
      caps["timeZone"] = "Tokyo"
      caps
    end
  end
end
