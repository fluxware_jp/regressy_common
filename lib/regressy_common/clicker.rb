module RegressyCommon
  module Clicker
    def retry_click(how, what, max=10)
      max.times do |c|
        if click_with_rescue(how, what)
          break
        end
        yield if block_given?
        if c >= max-1
          message = "retry_click count is over. how:#{how}, what:#{what}, count:#{c}"
          raise Selenium::WebDriver::Error::UnknownError, message
        end
        sleep 1
      end
    end

    def click_with_rescue(how, what)
      @driver.find_element(how, what).click
      true
    rescue => e
      logger.info "#{e.inspect}, at click_with_rescue"
      false
    end

    def failure_message(*params)
      "#{caller[0][/`([^']*)'/, 1]} is failed. #{params}"
    end
  end
end
