require 'tempfile'
module RegressyCommon
  class SnapShot
    def initialize(driver, tag)
      @driver = driver
      @tag = tag
    end

    def take_and_store
      save_screenshot
      RegressyCommon::SnapStorage.new.store_file(snap_file, @tag)
      true
    end

    private

    def snap_file
      @snap_file ||= Tempfile.create([snap_file_prefix, '.png'])
    end

    def snap_file_prefix
      (@tag.empty?)? "snap_" : %(snap_#{@tag}_)
    end

    def save_screenshot
      @driver.save_screenshot(snap_file.path)
    end
  end
end
