module RegressyCommon
  module Selector
    def select_option_by_text (how, what, content)
      element = @driver.find_element(how, what)
      Selenium::WebDriver::Support::Select.new(element).select_by(:text, content)
    end
  end
end
