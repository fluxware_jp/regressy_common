module RegressyCommon
  module Sender
    def send_keys(how, what, content)
      target_element(how, what).send_keys(content)
    end

    def jsend_keys(how, what, content)
      @driver.execute_script("arguments[0].value = arguments[1];", target_element(how, what), content)
    end

    def jsend_keys_with_keyinput (how, what, content)
      @driver.execute_script("arguments[0].value = arguments[1];", target_element(how, what), content)
      # for application listen to key_up/down
      target_element(how, what).send_keys(" ")
      target_element(how, what).send_keys(:backspace)
    end

    private

    def target_element(how, what)
      @driver.find_element(how, what)
    end
  end
end
