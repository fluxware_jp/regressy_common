require 'active_support/all'
module RegressyCommon
  module Checker
    def assert_element_exists(how, what, timeout_value=10)
      assert_with_timeout(failure_message(how, what), timeout_value) do
        (@driver.find_element(how, what))? true : false
      end
    end

    def assert_element_present(how, what, timeout_value=10)
      failure_message = failure_message(how, what)
      assert_elements_present(how, what, 0, timeout_value, failure_message: failure_message)
    end

    def assert_elements_present(how, what, index, timeout_value=10,  **options)
      options = options&.symbolize_keys
      failure_message = options[:failure_message] || failure_message(how, what, index)
      assert_elements_check_method(how, what, index, :displayed?, failure_message, timeout_value)
    end

    def assert_element_enabled(how, what, timeout_value=10)
      failure_message = failure_message(how, what)
      assert_elements_enabled(how, what, 0, timeout_value, failure_message: failure_message)
    end

    def assert_elements_enabled(how, what, index, timeout_value=10,  **options)
      options = options.symbolize_keys
      failure_message = options[:failure_message] || failure_message(how, what, index)
      assert_elements_check_method(how, what, index, :enabled?, failure_message, timeout_value)
    end

    def element_exists_no_assert?(how, what, timeout_value=10)
      timeout_value.times do |count|
        elements = @driver.find_elements(how, what)
        return true if (elements&.length > 0)  ## success
        sleep 1
      end
      false
    end

    def element_present_no_assert?(how, what, timeout_value=10)
      timeout_value.times do |count|
        elements = @driver.find_elements(how, what)
        return true if (elements&.length > 0 && elements[0].displayed?)  ## success
        sleep 1
      end
      false
    end

    private

    def assert_elements_check_method(how, what, index, check_method, failure_message, timeout_value=10)
      assert_with_timeout(failure_message, timeout_value) do
        result = false
        timeout_value.times do
          elements =  @driver.find_elements(how, what)
          (result = true) && break if elements[index]&.send(check_method)
          sleep 1
        end
        result
      end
    end

    def assert_with_timeout(message, timeout_value=10)
      @test_case.instance_eval do  # assert count 集計のため、test_case で実行する
        assert_block message do
          begin
            @driver.manage.timeouts.implicit_wait = 0  # reset every
            Selenium::WebDriver::Wait.new(timeout: timeout_value).until do
              yield
            end
          rescue => e
            logger.debug e.message
            logger.debug "timeout_value: #{timeout_value}"
            false
          end
        end
      end
    end

    def failure_message(*params)
      "#{caller[0][/`([^']*)'/, 1]} is failed. #{params}"
    end
  end
end
