require "logger"
module RegressyCommon
  module Utils
    module StandardLogger
      @@logger ||= Logger.new(STDOUT)
      def self.instance
        @@logger
      end
    end
  end
end
